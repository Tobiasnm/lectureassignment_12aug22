public class ElectricGuitar extends Guitar{
    boolean hasPickup;

    ElectricGuitar(String _brand, String _model, int _manufactureYear, boolean _hasPickup) {
        super(_brand, _model, _manufactureYear);
        hasPickup = _hasPickup;
    }

    @Override
    public void makeSound() {
        if (hasPickup){
            System.out.println("riff raff, but with a pickup (?)");
        }
        else System.out.println("riff raff");

    }

    public void test(){
        System.out.println("halal");
    }
}
