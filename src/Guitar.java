public abstract class Guitar implements Playable {
    Guitar(String _brand, String _model, int _manufactureYear){
        brand = _brand;
        model = _model;
        yearManufactured = _manufactureYear;
    }
    String brand;
    String model;
    int yearManufactured;

}
