import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        ArrayList<Guitar> guitars = new ArrayList();
        guitars.add(new ElectricGuitar("Mustang", "3",1992, true));
        guitars.add(new ElectricGuitar("BMW", "1",1328, false));
        guitars.add(new AcousticGuitar("Western", "Classic", 2022));
        ElectricGuitar el = new ElectricGuitar("asd", "asd", 123, true);
        guitars.add(el);

        guitars.forEach(guitar -> {
            System.out.println("Brand: " + guitar.brand + " Model: " + guitar.model + " Manufactured in: " + guitar.yearManufactured);
            guitar.makeSound();
        });
    }
}
