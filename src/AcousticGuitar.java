public class AcousticGuitar extends Guitar{

    AcousticGuitar(String _brand, String _model, int _manufactureYear) {
        super(_brand, _model, _manufactureYear);
    }

    @Override
    public void makeSound() {
        System.out.println("makes acoutstic guitar noises");
    }
}
